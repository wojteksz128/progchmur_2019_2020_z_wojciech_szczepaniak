#!/bin/bash

sh download_submodules.sh
sh compile_app.sh
sh start_hadoop_cluster.sh

vagrant ssh hadoop-master -c "cd scripts/app; sh run_app.sh"
