#!/bin/bash

echo "-------- run_app --------"

sh hadoop_start.sh
sh hdfs_mkdir_input.sh
sh hdfs_put_input.sh
sh start_finger_stdDev_app.sh
sh hdfs_get_output.sh
cat /home/vagrant/output/*
